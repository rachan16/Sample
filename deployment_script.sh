#!/bin/bash

echo "Please select the base URL for deployment:"
echo "1. Development (DEV)"
echo "2. Production (PROD)"
echo "3. QA (QA)"
read -p "Enter your choice (1-3): " choice

if [[ "$choice" == "1" ]]; then
  export BASE_URL=$BASE_URL_DEV
elif [[ "$choice" == "2" ]]; then
  export BASE_URL=$BASE_URL_PROD
elif [[ "$choice" == "3" ]]; then
  export BASE_URL=$BASE_URL_QA
else
  echo "Invalid choice. Exiting..."
  exit 1
fi

echo "Deploying to $BASE_URL"
echo "Done"
# Your deployment commands here using the selected base URL
